<?php


$app->group(['prefix' => 'v1', 'namespace' => 'App\Http\Controllers'], function ($app) {
    $app->get('users/{id}', 'UserController@getUser');
    $app->post('user', 'UserController@createUser');
    $app->get('users' , 'UserController@getUsers');
});

//$app->get('products', 'ExampleController@getProducts');
