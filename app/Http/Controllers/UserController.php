<?php
/**
 * Created by PhpStorm.
 * User: cesar
 * Date: 2/24/17
 * Time: 3:15 PM
 */

namespace App\Http\Controllers;

/**
 * @SWG\Swagger(
 *      schemes={"http"},
 *      basePath="/v1",
 *      @SWG\Info(
 *         version="1.0.0",
 *         title="Swagger Neoauto",
 *         @SWG\License(name="MIT")
 *      ),
 *      host="api.neoauto.com:8081",
 *      consumes={"application/json"},
 *      produces={"application/json"}
 * )
 */
use App\Model\User;
use Illuminate\Auth\Access\Response;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * @SWG\Get(
     *     path="/users/{id}",
     *     summary="Muestra los datos de un usuario",
     *     produces={"application/json"},
     *     @SWG\Response(
     *          response="200",
     *          description="successful operation"
     *      ),
     *     @SWG\Parameter(
     *          description="id del usuario",
     *          in="path",
     *          name="id", required=true, type="integer"),
     *     tags={
     *         "User"
     *     }
     * )
     *
     */
    public function getUser($id)
    {
        $user = User::find($id);
        return $user;
    }

    /**
     * @SWG\Post(
     *   path="/user",
     *   tags={
     *     "User"
     *   },
     *   summary="Crear usuarios",
     *   description="Esto solo puede ser hecho si el usuario esta logeado.",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     in="body",
     *     name="body",
     *     description="Creacion de un objeto usuario",
     *     required=true,
     *     @SWG\Schema(ref="#/definitions/User")
     *   ),
     *   @SWG\Response(response=200, description="successful operation")
     * )
     */

    public function createUser(Request $request)
    {
        try {

            $user = new User();
            $user->username     = $request->username;
            $user->password     = $request->password;
            $user->email        = $request->email;
            $user->names        = $request->names;
            $user->age          = $request->age;
            $user->userStatus   = $request->userStatus;
            $user->save();

            $response = [
                    'error'     => false ,
                    'message'   => 'Save Success',
                    'code'      => 1,
                    'data'      => [$user]

            ];

            return response()->json($response);

        }catch (\Exception $e){

            $response = [
                'error'     => true ,
                'message'   => 'Save Fail',
                'code'      => 1,
                'data'      => $e->getMessage()
            ];

            return response()->json($response);
        }

    }

    /*
     * @SWG\Get(
     *   path="/users",
     *   tags={
     *   "User"
     *   },
     *   description="Retorna todos los usuarios",
     *   operationId="getUsers",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *       name="limit",
     *       in="query",
     *       description="retorna un numero maximo de resultados",
     *       required=false,
     *       type="integer",
     *       format="int32"
     *   ),
     *   @SWG\Response(
     *       response=200,
     *       description="lista de users",
     *       @SWG\Schema(
     *           type="array",
     *           @SWG\Items(ref="#/definitions/User")
     *       )
     *   )
     * )
     */

    public function getUsersd(Request $request)
    {
        $limit = $request->limit;
        if($limit){
            $users  = User::limit($limit)->get();
        }else {
            $users = User::all();
        }
        return $users;
    }

    public function getUsers()
    {
        $users = User::all();
        return $users;
    }
}
