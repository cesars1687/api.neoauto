<?php

namespace App\Http\Controllers;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;

class ExampleController extends Controller
{

    public function __construct()
    {
        //
    }

    /*
     * @SWG\Get(path="/products", summary="Lista todos os produtos", produces={"application/json"},
     *      @SWG\Response(response="default", description="successful operation"),
     *      @SWG\Parameter(description="Pesquisa", in="query", name="like", required=false, type="string"))
     */
    public function getProducts(Request $request)
    {
        $products = new Collection();

        $products->push(['name' => 'Galaxy S6', 'price' => 10]);
        $products->push(['name' => 'Moto G', 'price' => 15]);
        $products->push(['name' => 'Redmi2 Pro', 'price' => 20]);

        if($request->has('like'))
        {
            $like = $request->get('like');

            $products = $products->filter(function ($item) use ($like){
                return strpos($item['name'], $like) !== false;
            });
        }

        return $products->all();
    }
    //
}
