<?php
/**
 * Created by PhpStorm.
 * User: cesar
 * Date: 2/24/17
 * Time: 4:44 PM
 */

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

/**
 * @SWG\Definition(type="object", @SWG\Xml(name="User"))
 */

class User extends Model
{
    protected $table = 'users';

    /**
     * @SWG\Property(format="int64")
     * @var int
     */
    protected $id;

    /**
     * @SWG\Propery(required=true)
     * @var string
     */
    protected $username;

    /**
     * @SWG\Property()
     * @var string
     */
    protected $names;

    /**
     * @var string
     * @SWG\Property()
     */
    protected $email;

    /**
     * @var string
     * @SWG\Property()
     */
    protected $password;

    /**
     * User Age
     * @var int
     * @SWG\Property(format="int32")
     */
    protected $age;

    /**
     * User Status
     * @var int
     * @SWG\Property(format="int32")
     */
    protected $userStatus;
}
